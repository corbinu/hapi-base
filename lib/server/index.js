var Hapi = require('hapi');

var server = {};

server.controllers = require('./controllers');

	server.setup = function(config) {
		server.controllers.setup(config);

		server.instance = new Hapi.Server();

		server.instance.connection({
			hostname: config.get('hostname'),
			port: config.get('port')
		});

		if (config.get('good')) {

			server.instance.register(
			{
				register: require('good'),
				options: {
					opsInterval: 1000,
					reporters: [
						{
							reporter: require('good-console'),
							args:[{ log: '*', response: '*' }]
						}
					]
				}
			},
			function (err) {
				if (err) return console.log(err);
			}
		);
	}

	server.instance.route([
		{
			method: 'GET',
			path: '/',
			config: {
				handler: function(request, reply) {
					reply.redirect(config.get('base') + '/');
				}
			}
		},
		{
			method: 'GET',
			path: config.get('base') + '/',
			config: server.controllers.base.config
		}
	]);
};

server.start = function(callback) {

	server.instance.start(function () {
		callback(server.instance.info);
	});

};

module.exports = server;
