var controllers = {};

controllers.base = require('./base');

controllers.setup = function(config, store) {
	controllers.base.setup(config, store);
};

module.exports = controllers;
