var baseController = {
	config: {}
};

baseController.setup = function(config, store) {

	baseController.config.handler = function (request, reply) {
		reply({
			'hello': 'Welcome to the ' + config.get('name') +  ' API',
			'version': 'v0'
		});
	};

};

module.exports = baseController;
