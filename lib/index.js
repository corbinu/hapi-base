var API = {};

API.config = require('./config');
API.server = require('./server');

API.setup = function() {
    API.server.setup(API.config);
};

API.start = function(callback) {
    API.server.start(callback);
};

module.exports = API;
