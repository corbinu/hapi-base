var nconf = require('nconf');

switch (process.env.NODE_ENV) {
    case 'production':
        nconf.file({ file: './config/production.json' });
        break;
    case 'staging':
        nconf.file({ file: './config/staging.json' });
        break;
    case 'test':
        nconf.file({ file: './config/test.json' });
        break;
    default:
        nconf.file({ file: './config/development.json' });
        break;
}

nconf.env().argv();

module.exports = nconf;
