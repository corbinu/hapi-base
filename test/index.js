var Lab = require('lab');
var Code = require('code');

var API = require('../lib');

var lab = exports.lab = Lab.script();
var expect = Code.expect;

lab.experiment('API', function () {

    lab.test('basic', function (done) {

        expect(API.config).to.be.a.object();
        expect(API.server).to.be.a.object();
        expect(API.setup).to.be.a.function();
        expect(API.start).to.be.a.function();

        done();
    });

});
