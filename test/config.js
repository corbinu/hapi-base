var Lab = require('lab');
var Code = require('code');

var config = require('../lib/config');

var lab = exports.lab = Lab.script();
var expect = Code.expect;

lab.experiment('Config', function () {

    lab.test('test config', function (done) {

        expect(config.get('name')).equal('Add App Name to Configs');
        expect(config.get('base')).equal('/api/v0');
        expect(config.get('port')).equal(1800);
        expect(config.get('good')).to.be.false();

        done();
    });

});
