var Lab = require('lab');
var Code = require('code');

var API = require('../../lib');
var models = require('../../lib/models');

var lab = exports.lab = Lab.script();
var expect = Code.expect;

lab.experiment('Models', function () {

    lab.test('basic', function (done) {

        expect(models.reset).to.be.a.function();

        done();
    });

});
