var Lab = require('lab');
var Code = require('code');

var API = require('../../../lib');
var controllers = require('../../../lib/server/controllers');

var lab = exports.lab = Lab.script();
var expect = Code.expect;

lab.experiment('Controllers', function () {

    lab.test('basic', function (done) {

        expect(controllers.base).to.be.an.object();
        expect(controllers.setup).to.be.a.function();

        done();
    });

    lab.test('setup', function (done) {

        controllers.setup(API.config, API.store);

        done();
    });

});
