var Lab = require('lab');
var Code = require('code');

var API = require('../../../lib');
var config = require('../../../lib/config');
var base = require('../../../lib/server/controllers/base');

var lab = exports.lab = Lab.script();
var expect = Code.expect;

lab.experiment('Base', function () {

    lab.test('basic', function (done) {

        expect(base.setup).to.be.a.function();

        done();
    });

    lab.test('setup', function (done) {

        base.setup(API.config, API.store);

        done();
    });

    lab.experiment('GET', function() {

        lab.before(function (done) {

            API.setup();

            done();
        });

        lab.test('welcome', function (done) {

            var options = {
                method: "GET",
                url: config.get('base') + '/'
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                expect(response.statusCode).equal(200);
                expect(result).to.be.an.object();
                expect(result.hello).equal('Welcome to the ' + config.get('name') +  ' API');
                expect(result.version).equal('v0');

                done();
            });
        });

    });
});
