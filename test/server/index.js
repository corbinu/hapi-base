var Lab = require('lab');
var Code = require('code');

var API = require('../../lib');
var server = require('../../lib/server');

var lab = exports.lab = Lab.script();
var expect = Code.expect;

lab.experiment('Server', function () {

    lab.test('basic', function (done) {

        expect(server.controllers).to.be.an.object();
        expect(server.instance).to.be.an.object();
        expect(server.setup).to.be.a.function();
        expect(server.start).to.be.a.function();

        done();
    });

    lab.test('setup', function (done) {

        server.setup(API.config, API.store);

        done();
    });

});
