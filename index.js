var API = require('./lib');

API.setup();

API.start(function (info) {
	console.log('HAPI Base API started on port: ', info.port);
});
